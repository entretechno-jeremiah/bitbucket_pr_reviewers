#!/usr/bin/env ruby

# Fetches all pull requests created by current Bitbucket user within the last
# month, and tallies how many pull requests/lines of code each reviewer was
# assigned.

require 'dotenv/load'
require 'net/http'
require 'json'
require 'time'
require 'base64'

IGNORED_PATHS =
  [
    'Gemfile.lock',
    'package-lock.json',
    'yarn.lock',
    'db/schema.rb',
    %r{\Adb/seeds/data/}
  ].freeze

def get(uri_str, params = {})
  uri = URI(uri_str)
  uri.query =
    URI.encode_www_form(
      URI.decode_www_form(uri.query || '').to_h.merge(
        params
      )
    )

  req = Net::HTTP::Get.new(uri)
  req.basic_auth(
    ENV['BITBUCKET_USERNAME'],
    ENV['BITBUCKET_PASSWORD'] # https://bitbucket.org/account/settings/app-passwords/
  )
  http = Net::HTTP.new(uri.host, uri.port)
  http.use_ssl = true
  resp = http.request(req)

  raise resp.error_type.new(nil, resp) unless resp.is_a?(Net::HTTPSuccess)

  resp_json = JSON.parse(resp.body)

  if resp_json['values']
    if resp_json['next']
      resp_json['values'] + get(resp_json['next'])
    else
      resp_json['values']
    end
  else
    resp_json
  end
end

one_month_ago = Time.now - (60 * 60 * 24 * (365.24225 / 12))
user = get('https://api.bitbucket.org/2.0/user')
reviewers = {}
pull_requests =
  get(
    "https://api.bitbucket.org/2.0/pullrequests/#{user['account_id']}",
    q:
      [
        "created_on >= #{one_month_ago.iso8601}",
        'source.branch.name != "master"',
        'source.branch.name != "staging"',
        'source.branch.name != "qa"',
        'source.branch.name != "qa-2"'
      ].join(' AND '),
    pagelen: 50
  )
pull_requests.map! do |pull_request|
  get(pull_request['links']['self']['href'])
end
pull_requests.each do |pull_request|
  pull_request['diffstat'] = get(pull_request['links']['diffstat']['href'])
end
pull_requests.each do |pull_request|
  pull_request['reviewers'].each do |reviewer|
    reviewers[reviewer['account_id']] ||= {}
    reviewers[reviewer['account_id']]['user'] ||= reviewer
    reviewers[reviewer['account_id']]['pullrequests'] ||= []
    reviewers[reviewer['account_id']]['pullrequests'] << pull_request
  end
end

paths = {}
pull_requests.each do |pull_request|
  pull_request['diffstat'].each do |file|
    if file['old']
      paths[file['old']['path']] ||= { num_pull_requests: 0, lines_added: 0, lines_removed: 0 }
      paths[file['old']['path']][:num_pull_requests] += 1
      paths[file['old']['path']][:lines_added] += file['lines_added']
      paths[file['old']['path']][:lines_removed] += file['lines_removed']
    end
    if file['new']
      paths[file['new']['path']] ||= { num_pull_requests: 0, lines_added: 0, lines_removed: 0 }
      unless file['old'] && file['old']['path'] == file['new']['path']
        paths[file['new']['path']][:num_pull_requests] += 1
      end
      paths[file['new']['path']][:lines_added] += file['lines_added']
      paths[file['new']['path']][:lines_removed] += file['lines_removed']
    end
  end
end

report_str = ''
reviewers
  .sort_by { |_reviewer_account_id, reviewer| reviewer['user']['display_name'].downcase }
  .each do |_reviewer_account_id, reviewer|
    lines_added = 0
    lines_removed = 0
    reviewer['pullrequests'].each do |pull_request|
      pull_request['diffstat'].each do |file|
        next if file['old'] && IGNORED_PATHS.any? { |ignored_path| ignored_path === file['old']['path'] }
        next if file['new'] && IGNORED_PATHS.any? { |ignored_path| ignored_path === file['new']['path'] }

        lines_added += file['lines_added']
        lines_removed += file['lines_removed']
      end
    end

    report_str <<
      <<~REVIEWER.gsub("\n", ' ').strip
        #{reviewer['user']['display_name']}:
        #{reviewer['pullrequests'].length} PRs
        (+#{lines_added}, -#{lines_removed})
      REVIEWER
    report_str << "\n"
  end

report_str << "\n"

paths
  .to_a
  .sort_by do |path, path_info|
    [
      path_info[:lines_added] * -1,
      path_info[:lines_removed] * -1,
      path_info[:num_pull_requests] * -1,
      path
    ]
  end
  .each do |path, path_info|
    report_str <<
      <<~REVIEWER.gsub("\n", ' ').strip
        #{path}:
        #{path_info[:num_pull_requests]} PRs
        (+#{path_info[:lines_added]}, -#{path_info[:lines_removed]})
        #{'[IGNORED]' if IGNORED_PATHS.any? { |ignored_path| ignored_path === path }}
      REVIEWER
    report_str << "\n"
  end

report_data_uri =
  "data:text/plain;charset=UTF-8;base64,#{Base64.encode64(report_str).gsub("\n", '')}"
system(
  'open',
  '-na',
  'Firefox Developer Edition.app',
  '--args',
  report_data_uri
)
